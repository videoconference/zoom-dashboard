import datetime
import logging
import time
import traceback

import requests
from requests.exceptions import HTTPError
import click

from utils.helper import helper 
from flask.cli import with_appcontext
from utils.zclient import UserComponent, ZoomAPIClient
from utils.openid import Openid
from config import ZOOM_BASE_URL, ZOOM_API_CLIENT, ZOOM_API_SECRET, KEYCLOAK_API_TOKEN_ENDPOINT, KEYCLOAK_ENDPOINT, AUTHZSVC_ENDPOINT, CLIENT_SECRET, CLIENT_ID, AUTHZ_ENDPOINT

logger = logging.getLogger('zoom-dashboard')

SUB_ACCOUNTS = {
    'zoom.sub01@cern.ch': 'zsub01',
    'zoom.sub02@cern.ch': 'zsub02',
    'ruben.gaspar.aparicio@cern.ch': 'rgaspar',
    'john.cassar@cern.ch': 'jcassar',
    'zoom.opert03@cern.ch': 'zopert03',
    'zoom.subtest@cern.ch': 'zsubtest'
}

@click.command()
@click.option("--sub", help='It is Zoom subaccount', is_flag=True)
@click.option("--group1000", help='people with 1000 Webinar add-on')
@click.option("--group500", help='people with 500 Webinar add-on')
@click.option("--days", help='number of days to be removed from license owner', type=int, default=90)
@click.option("--capacity", help='among 500 or 1000', type=int, default=500)
@with_appcontext
def populate_firstime_group(sub, group1000, group500, days, capacity):
    """ Populate with accounts, the grappa group.

    """

    openid = Openid()
    groupid1000=openid.get_group_id(group1000)
    logger.info('{} id is: {}'.format(group1000, groupid1000))

    groupid500=openid.get_group_id(group500)
    logger.info('{} id is: {}'.format(group500, groupid500))

    members1000=[]
    openid.get_allmembers(groupid1000, members1000, fields=['primaryAccountEmail','upn'])
    members500=[]
    openid.get_allmembers(groupid500, members500, fields=['primaryAccountEmail','upn'])
    
    lastwebinar=_last_zoom_webinar('past', 6)
    if lastwebinar == -1:
        logger.debug("error retrieving whole data set of webinars, we cant compare")
        return
    for zaccount in lastwebinar:
        if (datetime.datetime.now() - datetime.datetime.strptime(lastwebinar[zaccount] , "%Y-%m-%dT%H:%M:%SZ")).days < days:
            upn = zaccount.replace("@cern.ch","")
            if sub:
                upn=SUB_ACCOUNTS[zaccount]
            
            account_id=openid.get_identity(upn)
            logger.info("UPN: {} identity: {}".format(upn, account_id))
            
            if capacity==1000 and not next((item for item in members1000 if item["upn"] == upn), None) and \
                not next((item for item in members500 if item["upn"] == upn), None):
                openid.add_id_to_group(groupid1000, account_id)
                logger.info("{} account added to group {}".format(upn, group1000))
            elif capacity==500 and not next((item for item in members500 if item["upn"] == upn), None) and \
                not next((item for item in members1000 if item["upn"] == upn), None):
                openid.add_id_to_group(groupid500, account_id)
            else:
                logger.info("{} account already in group".format(upn))
            # change webinar add-on
            try: 
                if (capacity == 1000 and next((item for item in members1000 if item["upn"] == upn), None)) or \
                    (capacity == 500 and next((item for item in members500 if item["upn"] == upn), None)):
                    _set_zoom_webinar(zaccount, capacity) 
            except HTTPError as ex:
                logger.debug('got exception: {} while working with user {}'.format(ex.response.json(),zaccount))
                if ex.response.json()['code'] == 200:
                    logger.debug('no more licenses with capacity {}'.format(capacity))
                    continue
        else:
            logger.info('condition not met by user {}'.format(zaccount))
            
                   



@click.command()
@click.option("--sub", help='It is Zoom subaccount', is_flag=True)
@click.option("--group1000", help='people with 1000 Webinar add-on')
@click.option("--days", help='number of days to be removed from license owner', type=int, default=90)
@with_appcontext
def global_action2(sub, group1000, days):
    """ Remove users from grappa group and set webinar add-on to false in case condition is met
    """

    openid = Openid()
    groupid1000=openid.get_group_id(group1000)
    logger.info('{} id is: {}'.format(group1000, groupid1000))

    members1000=[]
    openid.get_allmembers(groupid1000, members1000, fields=['primaryAccountEmail','upn'])
    
    lastwebinar=_last_zoom_webinar('past', 6)
    if lastwebinar == -1:
        logger.debug("error retrieving whole data set of webinars, we cant compare")
        return
    for member in members1000:
        zaccount='{}@cern.ch'.format(member['upn'])
        if sub:
            zaccount=member['primaryAccountEmail']
        logger.info('working with account: {}'.format(zaccount))
        if zaccount not in lastwebinar:
            # we cant remove the license as it may be just attributed
            logger.debug('Account {} has no webinar so far.'.format(zaccount))
        else:
            logger.debug('Last webinar for account {} was at {}'.format(zaccount, lastwebinar[zaccount]))
        if (datetime.datetime.now() - datetime.datetime.strptime(lastwebinar.get(zaccount, datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")) , "%Y-%m-%dT%H:%M:%SZ")).days > days:
            account_id=openid.get_identity(member['upn'])
            logger.debug("UPN: {} identity: {}".format(member['upn'], account_id))
            openid.remove_id_from_group(groupid1000, account_id)
            logger.debug("{} account removed from group {}".format(member['upn'], group1000))
            # change webinar add-on
            #_set_zoom_webinar(zaccount, 0)

@click.command()
@with_appcontext
def list_zoom_accounts():
    """
    Get all accounts email and cernmailupn listed
    """
    logger.info("Starting script")
    zoomapi = ZoomAPIClient(ZOOM_API_CLIENT, ZOOM_API_SECRET, ZOOM_BASE_URL)
    data = {
        "page_size": 300
    } 
    users=[]
    _iterate_pages(zoomapi, data, users)
    return users

def _iterate_pages(zoomapi, data, users):
    
    result = zoomapi.list_users(**data)
    if len(result.get('users', [])) > 0:
        for user in result['users']:
            user_email = user['email']
            if user['email'] in users:
                logger.debug('strange condition, user: {} already retrieved'.format(user['email']))
            else:
                users.append(user_email)
            print("Email is {}".format(user_email))
        if result['next_page_token']:
            data['next_page_token']=result['next_page_token']
            _iterate_pages(zoomapi, data, users)
    else:
        logger.error('Couldnt get the list of Zoom users')


@click.command()
@with_appcontext
def get_alluser_webinar():
    # all users at CERN Zoom
    logger.info("Starting script")
    zoomapi = ZoomAPIClient(ZOOM_API_CLIENT, ZOOM_API_SECRET, ZOOM_BASE_URL)
    data = {
        "page_size": 300
    } 
    users=[]
    _iterate_pages(zoomapi, data, users)
    logger.info('we retrieved {} users'.format(len(users)))
    #users= ['pgorniak@cern.ch','paferrar@cern.ch','hosokawa@cern.ch','gingrich@cern.ch']
    users_webinar=[]
    for user in users:
        logger.info('working with user {}'.format(user))
        data={
            "id": user
        }
        ret=zoomapi.get_webinar_addon(**data)
        if ret.get('feature').get('webinar'):
            users_webinar.append(user)
    logger.info('{} users have webinar enabled'.format(len(users_webinar)))
    return users_webinar




@click.command()
@click.option("--group1000", help='people with 1000 Webinar add-on')
@click.option("--group500", help='people with 500 Webinar add-on')
@click.option("--sub", help='It is Zoom subaccount', is_flag=True)
@with_appcontext
def find_missing_webinarlicenses(group1000, group500, sub=False):
    """We try to see mismatch use of Webinar licenses

    Args:
        group1000 ([str]): Grappa group with users entitled to a webinar license
        group500 ([str]): Grappa group with users entitled to a webinar license
        sub ([boolean]): In case working with subaccount
    """
    # all users at CERN Zoom
    logger.info("Starting script")
    zoomapi = ZoomAPIClient(ZOOM_API_CLIENT, ZOOM_API_SECRET, ZOOM_BASE_URL)
    data = {
        "page_size": 300
    } 
    users=[]
    _iterate_pages(zoomapi, data, users)
    logger.info('we retrieved {} users'.format(len(users)))
    #users= ['pgorniak@cern.ch','paferrar@cern.ch','hosokawa@cern.ch','gingrich@cern.ch']
    users_webinar=[]
    
    for user in users:
        data={
            "id": user
        }
        try: 
            ret=zoomapi.get_webinar_addon(**data)
        except requests.exceptions.HTTPError as ex:
            logger.warn(ex)
            continue
        if ret.get('feature').get('webinar'):
            users_webinar.append(user)
            logger.info('user: {} has webinar license'.format(user))
        else:
            logger.info('user: {} has not webinar license'.format(user))
    logger.info('{} users have webinar enabled'.format(len(users_webinar)))   
    
    # retrieve all users webinar1000
    openid = Openid()
    groupid1000=openid.get_group_id(group1000)
    logger.info('{} id is: {}'.format(group1000, groupid1000))
    groupid500=openid.get_group_id(group500) 
    logger.info('{} id is: {}'.format(group500, groupid500))

    members1000=[]
    members1000zoom=[]
    openid.get_allmembers(groupid1000, members1000, fields=['primaryAccountEmail','upn'])
    openid.get_allmembers(groupid500, members1000, fields=['primaryAccountEmail','upn'])
    for member in members1000:
        if sub:
            members1000zoom.append('{}'.format(member['primaryAccountEmail']))
        else:
            members1000zoom.append('{}@cernch'.format(member['upn']))
        

    logger.info("Missing values in the Zoom compare to Openid group: {}".format(set(members1000zoom).difference(users_webinar)))
    logger.info("Missing values in OpenidGroup compared to Zoom: {}".format(set(users_webinar).difference(members1000zoom)))
    



@click.command()
@click.option("--sub", help='It is Zoom subaccount', is_flag=True)
@click.option("--group500", help='people with 500 Webinar add-on')
@click.option("--group1000", help='people with 1000 Webinar add-on')
@click.option("--days", help='number of days to be removed from license owner', type=int, default=30)
@with_appcontext
def global_action(sub, group500, group1000, days):

    openid = Openid()
    groupid500=openid.get_group_id(group500)
    logger.info('{} id is: {}'.format(group500, groupid500))
    groupid1000=openid.get_group_id(group1000)
    logger.info('{} id is: {}'.format(group1000, groupid1000))

    members1000=[]
    openid.get_allmembers(groupid1000, members1000, fields=['primaryAccountEmail','upn'])
    members500=[]
    openid.get_allmembers(groupid500, members500, fields=['primaryAccountEmail','upn'])

    for member in members1000:
        print(member)
        zaccount='{}@cern.ch'.format(member['upn'])
        if sub:
            zaccount=member['primaryAccountEmail']
        try: 
            if _list_webinars(zaccount):
                continue
        except requests.exceptions.HTTPError as ex:
            logger.debug("Could not retrieve webinars for {} exception: {}".format(zaccount, ex))
            continue
    
        #remove from group 1000 and add to 500
        logger.debug('{} needs to be removed from: {}'.format(member['upn'], group1000))
       
        account_id=openid.get_identity(member['upn'])
        if next((item for item in members1000 if item['upn'] == member['upn']), None):
            openid.remove_id_from_group(groupid1000, account_id)
            logger.debug("{} account removed from group {}".format(member['upn'], group1000))
        if not next((item for item in members500 if item['upn'] == member['upn']), None):
            openid.add_id_to_group(groupid500,account_id)
            logger.debug("{} account added to group {}".format(member['upn'], group500))
        else:
            logger.debug("{} already a member of group {}".format(member['upn'], group500))
            _set_zoom_webinar(zaccount, 500)
    
    lastwebinar=_last_zoom_webinar('past', 6)
    if lastwebinar == -1:
        logger.debug("error retrieving whole data set of webinars, we cant compare")
        return
    for member in members500:
        zaccount='{}@cernch'.format(member['upn'])
        if sub:
            zaccount=member['primaryAccountEmail']
        if (datetime.datetime.now() - datetime.datetime.strptime(lastwebinar.get('zaccount', datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")) , "%Y-%m-%dT%H:%M:%SZ")).days > days:
            account_id=openid.get_identity(member['upn'])
            logger.debug("UPN: {} identity: {}".format(member['upn'], account_id))
            openid.remove_id_from_group(groupid500, account_id)
            logger.debug("{} account removed from group {}".format(member['upn'], group500))
            # change webinar add-on
            _set_zoom_webinar(zaccount, 0)


@click.command()
@click.option("--group", help='group to retrieve members from')
@click.option("--account", help='Zoom account to be added e.g. toto@cern.ch')
@with_appcontext
def get_members_add_member(group, account):
    
    openid = Openid()
    
    groupid=openid.get_group_id(group)
    members = []
    openid.get_allmembers(groupid, members)
    logger.debug("members length {}".format(len(members)))

    #find identity for account
    if account:
        account_parts=account.split('@')
        account_id=openid.get_identity(account_parts[0])
        if not next((member for member in members if member['upn'] == account_parts[0]), None):
            openid.add_id_to_group(groupid,account_id)
            logger.debug("{} account added to group {}".format(account_parts[0], group))
        else:
            logger.debug("{} already a member of group {}".format(account_parts[0], group))
    

@click.command()
@click.option("--user", help='Zoom user to retrieve webinars')
@with_appcontext
def list_webinars(user):
    logger.info("Starting script")

    return _list_webinars(user)

def _list_webinars(user):
    zoomapi = ZoomAPIClient(ZOOM_API_CLIENT, ZOOM_API_SECRET, ZOOM_BASE_URL)
    data = {
        'page_size': 300,
        'id': user
    }
    ret = zoomapi.list_user_webinars(**data)
    for item in ret['webinars']:
        print(item)
        if item['type'] == 5  and datetime.datetime.strptime(item['start_time'], "%Y-%m-%dT%H:%M:%SZ") > datetime.datetime.now():
            print('got here')
            if ('1000attendees' in item.get('agenda', '') or '1000attendees' in item.get('topic', '')):
                print(item)  
                return True
        elif item['type'] == 9:
            webinarobj = zoomapi.get_webinar_details(**{'webinarid': item['id']})
            for oc in webinarobj.get('occurrences', None):
                if datetime.datetime.strptime(oc['start_time'], "%Y-%m-%dT%H:%M:%SZ") > datetime.datetime.now() and \
                    ('1000attendees' in item.get('agenda', '') or '1000attendees' in item.get('topic', '')):
                    print(webinarobj)
                    return True               

    return False

@click.command()
@click.option("--past", help='if present its about past webinars', is_flag=True)
@click.option("--interval", help='number of months', type=int)
@with_appcontext
def last_zoom_webinar(past, interval):
    logger.info("Starting script")
    return _last_zoom_webinar(past,interval)
    

def _last_zoom_webinar(past, interval):
    zoomapi = ZoomAPIClient(ZOOM_API_CLIENT, ZOOM_API_SECRET, ZOOM_BASE_URL)

    lastwebinar={}
    if past:
        typeofevent = 'past'
    else:
        typeofevent = 'live'
    data = {
        'type': typeofevent,
        'page_size': 300
    }    

    while interval > 0:
        data['from']=(datetime.date.today() - datetime.timedelta(days=interval*30)).strftime("%Y-%m-%d")
        if interval == 0:
            data['to']=datetime.date.today().strftime("%Y-%m-%d")
        else:
            data['to']=(datetime.date.today() - datetime.timedelta(days=(interval-1)*30)).strftime("%Y-%m-%d")
        print(data['from'])
        print(data['to'])
        try: 
            ret = _get_live_events(zoomapi, data=data.copy())
        except:
            return -1
        interval=interval-1
        if ret == None or len(ret)==0:
            logger.warn("No values return in this iteration. HTTP Exception")
            break
        for item in ret:
            print(item)
            if 'end_time' in item.keys():
                if item['email'] in lastwebinar.keys():
                    if datetime.datetime.strptime(item["end_time"], "%Y-%m-%dT%H:%M:%SZ") > \
                    datetime.datetime.strptime(lastwebinar[item['email']], "%Y-%m-%dT%H:%M:%SZ"):
                        lastwebinar[item['email']] = item['end_time']
                else:
                    lastwebinar[item['email']] = item['end_time']
        logger.info("total number of entries retrieved from: {} to: {} value: {}".format(data['from'], data['to'],len(ret)))
    logger.info(lastwebinar)
    return  lastwebinar

def _get_live_events(zoom, data):
    """
    Iteration to retrieve all events
    """
    events = []
    try: 
        res=zoom.list_webinars(**data)
        events = res["webinars"]
        print("events {}".format(events))
        counter = 1
        while res["next_page_token"] != '':
            if counter > 9:
                counter = 1
                time.sleep(120)
                logger.warn("We need to go sleep as going above 10 calls")
            data["next_page_token"] = res["next_page_token"]
            res = zoom.list_webinars(**data)
            events.extend(res["webinars"])
            counter += 1
    except requests.exceptions.HTTPError as ex: 
        logger.error(ex)
        raise
    except:
        logger.error("Unexpected exception: {}".format(traceback.format_exc()))  
        raise
    return events

@click.command()
@click.option("--account", help='id of the account to get')
@click.option("--capacity", help='webinar capacity', type=int)
@with_appcontext
def set_zoom_webinar(account, capacity):
    """[summary]

    Args:
        account ([string]): id of the account e.g. email or id
        capacity ([int]): capacity of the webinar, if > 0 it will be set
    """
    logger.info("Starting script")
    _set_zoom_webinar(account, capacity)
    
    

def _set_zoom_webinar(account, capacity):
    zoomapi = ZoomAPIClient(ZOOM_API_CLIENT, ZOOM_API_SECRET, ZOOM_BASE_URL)
    logger.info("working with account: <{}>".format(account))
    data = {}
    if capacity > 0 and capacity in [500, 1000]:
        data = {            
            "id": account,
            "feature": {
                "webinar": True,
                "webinar_capacity": capacity
            }
        }
    else:
        data = {            
            "id": account,
            "feature": {
                "webinar": False,
            }
        }
    result = zoomapi.set_webinar_addon(**data)
    if result.status_code == 204:
        logger.info('User: {} was updated'.format(account))
    else:
        logger.error('User: {} couldnt be found. Error: {}'.format(account, result))